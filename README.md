# README #

### Uruchomienie i instalacja aplikacji ###

* git clone https://Rakky96@bitbucket.org/Rakky96/zadrekrutacyjne.git 
* composer install
* php artisan serve
* php artisan db:create zadRekrutacyjne
* W pliku .env, należy ustawić nazwę bazy jeśli została nadpisana przez wartość domyślną: DB_DATABASE=zadRekrutacyjne
* php artisan migrate
* php artisan db:seed
* php artisan optimize:clear

### Aplikacja została stworzona i uruchomiona w: ###

* composer version 2.0.12
* php version 7.3.26
* xampp control panel v3.2.4

### Sposób działania i walidowania ###

* Gdy są błędy pojawi się komunikat, jeśli nie ma wyszukać zostaje zwrócona pusta tablica, 
* w przypadku poprawnego wyszukania zostanie zwrócona tablica obiektów.
* Jest to zabieg celowy ponieważ jakbym chciał wypisać jakiś komunikat w przypadku pustej tablicy,
* to sprawdzałbym to w twigu i tam wyświetlał komunikat.