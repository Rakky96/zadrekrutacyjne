<?php

namespace App\Services;

use Validator;
use App\Repositories\RecordRepository;
use Illuminate\Http\Request;

class RecordService
{   
    public function __construct(Request $request, RecordRepository $recordRepo){
        $this->request = $request;
        $this->recordRepo = $recordRepo;
    }

    /**
    * Check the same parameters in url
    *
    * @return bool
    */
    public function validateCountSameParam()
    {
        $fullUrl = $this->request->getRequestUri();
        $fullUrl = substr($fullUrl, 2);
        $arrayWithParamUrl = explode("&", $fullUrl);
   
        $idCount = 0;
        $titleCount = 0;
        $descriptionCount = 0;
        $created_atCount = 0;
        // Count the same param in url
        foreach ($arrayWithParamUrl as $key => $val) {
            if(strpos($val, 'id') !== false) $idCount++;
            elseif(strpos($val, 'title') !== false)  $titleCount++;
            elseif(strpos($val, 'description') !== false) $descriptionCount++;
            elseif(strpos($val, 'created_at') !== false) $created_atCount++;
        }

        if($idCount > 1 || $titleCount > 1 || $descriptionCount > 1 || $created_atCount > 1) {
            return false;
        }else return true;
    }

    /**
    * Get records from db
    *
    * @return string
    */
    public function getRecords()
    {
        $url =  $this->request->all();
        // Will only accept alpha and spaces.
        Validator::extend('alpha_spaces', function ($attribute, $value) {
            return preg_match('/^[\pL\s]+$/u', $value); 
        });
        //Validate url param
        $validator = Validator::make($url, [
            'id' => 'integer|min:1|max:99999',
            'title' => 'string|min:1|max:255|alpha_spaces',
            'description' => 'string|min:1|max:255|alpha_spaces',
            'created_at' => 'date_format:Y-m-d|after_or_equal:2021-10-10'
        ]);

        if (!$validator->fails() && is_array($url)){
            $data = $this->recordRepo->getRecord($url)->all();
            if(!empty($data)){
                return response()->json($data, 200);
            }else return  $this->messageError();
            
        }else return  $this->messageError();
    }

    /**
    * Return message error
    *
    * @return string
    */
    public function messageError(){
        $message =  'Nie znaleziono rekordów<br>
                    Błędny adres url, spróbuj skorzystać z przykładowego, prawidłowego zapisu: <br>
                    /?id=1&title=tytul&description=opis&created_at=yyyy-mm-dd<br>
                    Użyj któregoś z dostępnych: id, title, description, created_at';
 
         return $message;
     }
}
