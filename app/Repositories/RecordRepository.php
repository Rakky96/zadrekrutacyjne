<?php 

namespace App\Repositories;

use App\Models\Record;

class RecordRepository extends BaseRepository{

    public function __construct(Record $model){
        $this->model = $model;
    }

    public function getRecord($arrayParams){
      return $this->model->where(function ($query) use ($arrayParams) {
            foreach ($arrayParams as $key => $value) {
                //Chck current param in url
                if(array_key_exists($key, $arrayParams)){
                    if($key == 'id'){
                        $query->where($key, 'like', "{$value}");
                    }elseif($key == 'created_at'){
                        $query->where($key, 'like', "{$value}%");
                    }elseif($key == 'title' || $key == 'description'){
                        $query->where($key, 'like', "%{$value}%");
                    }else{
                        echo 'Nie znaleziono rekordów<br>
                              Nieprawidłowa nazwa paremetru<br>';
                        exit();
                    }
                }
            }
        })->get();
    }
}