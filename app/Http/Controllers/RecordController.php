<?php

namespace App\Http\Controllers;

use App\Services\RecordService;

class RecordController extends Controller
{

    public function __construct(RecordService $recordServie){
        $this->recordServie = $recordServie;
    }

    public function  searchStoreRecord(){
        $validateCountSameParam = $this->recordServie->validateCountSameParam();
        if($validateCountSameParam){
            return $this->recordServie->getRecords();
        }else{
            return  'Nie znaleziono rekordów<br>
                    Zbyt duża ilość takich samych pól';
        }
    }
}
